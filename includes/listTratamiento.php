<?php
	include'conexion.php';
	  
	$consulta = mysql_query("SELECT email as lis FROM LABORATORIO") or die (mysql_error());

	echo "<table>";  
	echo "";  
	echo "";  

	echo "<select name='nombre'>";  
	/*Con eso creamos una tabla y el select, tenemos que poner los "echo" para no cerrar el codigo php y además noten que lo que va dentro de las comillas dobles, por ejemplo el "name" del select, va dentro de comillas simples, si ponen comillas dobles, no funcionará.*/  
	  
	while($row = mysql_fetch_array($consulta)) {  
	/*Acá creamos un ciclo, donde la condición es algo como "Mientras existan registros seleccionados" y ademas creamos un arreglo(array) con estos registros, con el nombre de $row*/  
	  
	    echo "<option value='". $row['lis'] ."'>".$row['lis']."</option>";  
	  
	}  
	/*Lo que sucederá dentro del ciclo es que creamos un elemento del listado que lleve por valor (value) el nombre recogido del registro y además mostraremos en la página este mismo nombre. Como notarán lo que mostraremos en pantalla está entre dos puntos, estos puntos sirven para concatenar o unir 2 cadenas de texto , en este caso el código de la tabla con la variable para mostrar. No es estrictamente necesario que se haga de esta manera, otra forma podría ser "<option value='$row[lis]'>$row['lis']</option>", pero se recomienda la primera.*/  
	  
	echo "</select>"; 

	echo "";  
	echo "";  
	echo "";  
	  
	/*Aca cerramos todo, el select, la celda, la columna y la tabla*/  
?>  