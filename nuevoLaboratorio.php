<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Orto Smile</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  
    <link href="css/style.css" rel="stylesheet">


    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <h1>Nuevo Laboratorio</h1>
    <div class="container">
        <form  align="center" action="../OrtoSmi/includes/insert.php" method="POST" enctype="multipart/form-data">
            <div  class="form-group col-md-4 col-md-offset-4">
                <label>Email</label>
                <input type="email" name="email" class="form-control" placeholder="ejemplo@gmail.com" required>
                <label>Dirección</label>
                <input type="text" name="direccion" class="form-control" placeholder="col.hidalgo" required>
                <label>Horario</label>
                <input type="time" name="horario" class="form-control" placeholder="" required>
                <label>Teléfono</label>
                <input type="text" name="tel" class="form-control" placeholder="" required>
                <label>Longitud</label>
                <input type="text" name="longitud" class="form-control" placeholder="" required>
                <label>Latitud</label>
                <input type="text" name="latitud" class="form-control" placeholder="" required>
                <br>
                <br>
                <a href="laboratorios.php">
                    <input class="btn btn-danger" type="button" value="Cancerlar" />
                </a>
                <input class="btn btn-primary" name="submit" type="submit" value="Registrar">
            </div>
        </form>
    </div>
    <footer>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

</body>
</html>