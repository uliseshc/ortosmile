<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Orto Smile</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/agency.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar">¿Quienes somos?</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">OrtoSmile</a>
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
       
    </header>
    <br>
    <br>
    <br>   
    <br>
    <section id="">
        <div class="container">
            <div class=  "fila">
                <div class="columna" onclik="location.href='quienesSomos.php'">
                    <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>
                    <h4>¿Quiénes somos?</h4>
                    <!-- <a href="quienesSomos.php">PRUEBA</a> -->
                </div>
                <div id="div_a_medida"><span>Ingreso a Curso</span><form action="pagina.php"><input type="text" name="boton" value="Ingresar"></input></form></div>
                <div class="columna">
                    <form method="link" action="CasosExito.php">
                        <button href="CasosExito.php" type="summit" class="btn btn-default btn-lg">
                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Casos de éxito
                        </button>
                    </form>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Encuestas y Recompensas
                    </button>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-film" aria-hidden="true"></span> Tips
                    </button>
                </div>
                <div class="columna">
                    <form method="link" action="laboratorios.php">
                        <button href="laboratorios.php" type="summit" class="btn btn-default btn-lg">
                            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Laboratorios
                        </button>
                    </form>
                </div>
            </div>
            <div class="fila">
                <div class="columna">
                    <form method="link" action="tratamiento.php">   
                        <button href="tratamiento   .php" type="summit" class="btn btn-default btn-lg">
                            <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> Tratamientos
                        </button>
                    </form>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Paciente del mes
                    </button>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Citas
                    </button>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> Contacto
                    </button>
                </div>
                <div class="columna">
                    <button type="button" class="btn btn-default btn-lg">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Perfil
                    </button>
                </div>
            </div>
        </div>
    </section>
    <footer>
       <!--  <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Your Website 2014</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

</body>

</html>